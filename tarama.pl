#/usr/bin/perl
=pod
多良間島本体
@Author きよっち
=cut
use File::Basename qw/basename dirname/;
use strict;

my @urlList = ();
open( my $fh, '<', dirname(__FILE__) . '/url_list.js' );
while( my $line = <$fh> )
{
    $line =~ s/\r//g;
    $line =~ s/\n//g;
    push( @urlList, $line );
}
close( $fh );

chdir( dirname(__FILE__) );

my $num = @urlList;
my $i = 0;
my $exec = '"./bin/phantomjs" --web-security=no "./seo.js"';
while(1)
{
    print '----' . "\n" . $urlList[$i] . "\n";
    my $setExec = $exec . ' "' .$urlList[$i] . '"';
    print $setExec . "\n";
    my $lines = `$setExec`;
    #print $lines;
    print "\n";
    $i ++;
    if( $i == $num ){
        $i = 0;
    }
}