/* =============================================================================
 * SEOツール本体
 * ========================================================================== */

//設定読み込み
phantom.injectJs('ini.js');

//パス処理
var system = require('system');
var path = system.args[0].replace( /seo\.js$/, '' );

//URL取得
var args = phantom.args;
var url = args[0]
console.log(url);
var webPage = require('webpage');
var page = webPage.create();
page.settings.resourceTimeout = 4000;
page.settings.userAgent = "IEだよ";
page.customHeaders = {
    "User-Agent" : "IEだよ", 
    "Referer" : ini['url']
};


page.onResourceTimeout = function(request) {
    console.log('timeout');
    phantom.exit();
};

page.open(url, function(status)
{
    console.log(status);
    console.log(page.plainText);
    page.render('test.png');
    phantom.exit();
});

